#!/bin/sh
[ $# -ne 1 ] && echo "$0 <hernel_file>" && exit
sudo qemu-system-x86_64 -m 1024 -kernel $1 -nographic -append "console=ttyS0 root=/dev/sda"  -hda  ext2.qcow -netdev tap,id=net1 -device e1000,netdev=net1 
